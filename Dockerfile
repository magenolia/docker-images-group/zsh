ARG BASE_IMAGE

# Use multi-stage build to take advantage of cached layers for common operations through image versions.
# Also prevents dependencies required to build to arrive to final image.
FROM zshusers/zsh:latest AS builder

# Ensure root user for build.
USER root

# Language default environment variables.
ENV LANG="en_US.UTF-8"
ENV LC_CTYPE="en_US.UTF-8"

# Global installation path for ohmyzsh. This variable should not be overridden.
ENV ZSH=/usr/share/oh-my-zsh

# Install basic tools for build stage.
RUN install_packages \
  ca-certificates \
  git \
  curl

# Install globally ohmyzsh, powerlevel10k theme and zsh-autosuggestions for all users.
RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" --unattended \
  && git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH}/custom/themes/powerlevel10k \
  && git clone --depth=1 https://github.com/zsh-users/zsh-autosuggestions.git ${ZSH}/custom/plugins/zsh-autosuggestions

# Build final image.
FROM ${BASE_IMAGE}

# Ensure root user for build.
USER root

# Install additional tools.
RUN install_packages \
  ca-certificates \
  git \
  curl \
  sudo \
  systemd \
  curl \
  vim \
  nano \
  netcat \
  ssh \
  iputils-ping

# Language default environment variables.
ENV LANG="en_US.UTF-8"
ENV LC_CTYPE="en_US.UTF-8"

# Global installation path for ohmyzsh. This variable should not be overridden.
ENV ZSH=/usr/share/oh-my-zsh

# Copy ohmyzsh (with powerlevel10k theme) from builder.
COPY --from=builder ${ZSH} ${ZSH}

# Copy preconfigured logo and template files for applying ohmyzsh and powerlevel10k theme to new users.
COPY context/.zshrc /etc/skel/
COPY context/.p10k.zsh /etc/skel/
COPY context/logo.sh /usr/share/
RUN ["chmod", "a+x", "/usr/share/logo.sh"]

# Apply template files to root user.
# Create root ohmyzsh cache dir, if not created yet.
RUN cp -fv /etc/skel/.zshrc /etc/skel/.p10k.zsh /root/ \
  && mkdir -p ~/.cache/oh-my-zsh

# - Create a default user with uid:gid => 1000:1000.
# - Add it to www-data, adm and sudo groups.
# - Make this user a sudoer without password.
ARG USER='user'
ARG USER_UID='1000'
ARG USER_MAIN_GROUP='user'
ARG USER_MAIN_GID='1000'
ARG USER_ADDITIONAL_GROUPS='www-data,adm,sudo'

RUN groupadd -f -g "${USER_MAIN_GID}" "${USER_MAIN_GROUP}" \
  && useradd -m -o -u "${USER_UID}" -N -g "${USER_MAIN_GROUP}" -G ${USER_ADDITIONAL_GROUPS} -s /usr/bin/zsh "${USER}" \
  && echo "${USER} ALL=(root) NOPASSWD:ALL" | sudo EDITOR='tee -a' visudo

# Configure a shared space where it is allowed to to put custom executable paths from other containers
# via shared volume.
RUN mkdir -p "/custom/bin"
ENV PATH="/custom/bin:${PATH}"
RUN ["chmod", "-R", "a+rx", "/custom/bin/"]
VOLUME /custom/bin

# Configure entrypoint and cmd.
COPY context/docker-entrypoint /docker-entrypoint
RUN ["chmod", "a+x", "/docker-entrypoint"]
ENTRYPOINT ["/docker-entrypoint"]
CMD ["/usr/bin/zsh", "-l"]

# Define user and workdir.
USER ${USER}:${USER_MAIN_GROUP}
WORKDIR /home/${USER}

