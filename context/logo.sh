#!/bin/sh

# Logo.
# Generated at: http://patorjk.com/software/taag/#p=display&f=Doh&t=Magenolia
# Adapt it to your needs.

set -e

setup_color() {
  RAINBOW=""
  RED=""
  GREEN=""
  YELLOW=""
  BLUE=""
  BOLD=""
  RESET=""

  # Only use colors if connected to a terminal
  if [ -t 1 ]; then
    RAINBOW="
      $(printf '\033[38;5;196m')
      $(printf '\033[38;5;202m')
      $(printf '\033[38;5;226m')
      $(printf '\033[38;5;082m')
      $(printf '\033[38;5;021m')
      $(printf '\033[38;5;093m')
      $(printf '\033[38;5;163m')
      $(printf '\033[38;5;196m')
      $(printf '\033[38;5;202m')
    "
    RED=$(printf '\033[31m')
    GREEN=$(printf '\033[32m')
    YELLOW=$(printf '\033[33m')
    BLUE=$(printf '\033[34m')
    BOLD=$(printf '\033[1m')
    RESET=$(printf '\033[m')
  fi
}

# shellcheck disable=SC2183  # printf string has more %s than arguments ($RAINBOW expands to multiple arguments)
print_motd() {
  setup_color

  printf '%s                               %s                 %s                    %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s                    %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%sMMMMMMMM               MMMMMMMM%s                 %s                    %s                    %s                  %s                 %slllllll %s  iiii  %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%sM:::::::M             M:::::::M%s                 %s                    %s                    %s                  %s                 %sl:::::l %s i::::i %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::::M           M::::::::M%s                 %s                    %s                    %s                  %s                 %sl:::::l %s  iiii  %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%sM:::::::::M         M:::::::::M%s                 %s                    %s                    %s                  %s                 %sl:::::l %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::::::M       M::::::::::M%s  aaaaaaaaaaaaa  %s   ggggggggg   ggggg%s    eeeeeeeeeeee    %snnnn  nnnnnnnn    %s   ooooooooooo   %s l::::l %siiiiiii %s  aaaaaaaaaaaaa   %s\n' ${RAINBOW} ${RESET}
  printf '%sM:::::::::::M     M:::::::::::M%s  a::::::::::::a %s  g:::::::::ggg::::g%s  ee::::::::::::ee  %sn:::nn::::::::nn  %s oo:::::::::::oo %s l::::l %si:::::i %s  a::::::::::::a  %s\n' ${RAINBOW} ${RESET}
  printf '%sM:::::::M::::M   M::::M:::::::M%s  aaaaaaaaa:::::a%s g:::::::::::::::::g%s e::::::eeeee:::::ee%sn::::::::::::::nn %so:::::::::::::::o%s l::::l %s i::::i %s  aaaaaaaaa:::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M M::::M M::::M M::::::M%s           a::::a%sg::::::ggggg::::::gg%se::::::e     e:::::e%snn:::::::::::::::n%so:::::ooooo:::::o%s l::::l %s i::::i %s           a::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M  M::::M::::M  M::::::M%s    aaaaaaa:::::a%sg:::::g     g:::::g %se:::::::eeeee::::::e%s  n:::::nnnn:::::n%so::::o     o::::o%s l::::l %s i::::i %s    aaaaaaa:::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M   M:::::::M   M::::::M%s  aa::::::::::::a%sg:::::g     g:::::g %se:::::::::::::::::e %s  n::::n    n::::n%so::::o     o::::o%s l::::l %s i::::i %s  aa::::::::::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M    M:::::M    M::::::M%s a::::aaaa::::::a%sg:::::g     g:::::g %se::::::eeeeeeeeeee  %s  n::::n    n::::n%so::::o     o::::o%s l::::l %s i::::i %s a::::aaaa::::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M     MMMMM     M::::::M%sa::::a    a:::::a%sg::::::g    g:::::g %se:::::::e           %s  n::::n    n::::n%so::::o     o::::o%s l::::l %s i::::i %sa::::a    a:::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M               M::::::M%sa::::a    a:::::a%sg:::::::ggggg:::::g %se::::::::e          %s  n::::n    n::::n%so:::::ooooo:::::o%sl::::::l%si::::::i%sa::::a    a:::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M               M::::::M%sa:::::aaaa::::::a%s g::::::::::::::::g %s e::::::::eeeeeeee  %s  n::::n    n::::n%so:::::::::::::::o%sl::::::l%si::::::i%sa:::::aaaa::::::a %s\n' ${RAINBOW} ${RESET}
  printf '%sM::::::M               M::::::M%s a::::::::::aa:::%sa gg::::::::::::::g %s  ee:::::::::::::e  %s  n::::n    n::::n%s oo:::::::::::oo %sl::::::l%si::::::i%s a::::::::::aa:::a%s\n' ${RAINBOW} ${RESET}
  printf '%sMMMMMMMM               MMMMMMMM%s  aaaaaaaaaa  aaa%sa   gggggggg::::::g %s    eeeeeeeeeeeeee  %s  nnnnnn    nnnnnn%s   ooooooooooo   %sllllllll%siiiiiiii%s  aaaaaaaaaa  aaaa%s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s            g:::::g %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %sgggggg      g:::::g %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %sg:::::gg   gg:::::g %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s g::::::ggg:::::::g %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s  gg:::::::::::::g  %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s    ggg::::::ggg    %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '%s                               %s                 %s       gggggg       %s                    %s                  %s                 %s        %s        %s                  %s\n' ${RAINBOW} ${RESET}
  printf '\n'
  printf '\n'
}

print_motd
